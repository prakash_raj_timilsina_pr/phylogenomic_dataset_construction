This tutorial is for the SSB "Interogating transcriptomic data from processing to analysis" workshop May 20, 2015

Disclaimer: all instructions were tested with python 2.7 on a macbook pro with OS X 10.6.8 and on linux (Debian and Ubuntu). They may have compatibility issue with other versions and windows.

*If you have trouble or things take too long, feel free to get results from this [directory](https://www.dropbox.com/sh/kodpgt0xmgjb4nc/AAB8dtzZ5aSAz53ieMLctp20a?dl=0).*