#Part I: Install dependencies

###If you are using windows (and possibly mac)
Here's instruction on how to install virtualbox, a virtual machine, on your machine http://www.biotnet.org/sites/biotnet.org/files/documents/230/installing_ubuntu.pdf. A virtual machine will allow you to run linux within your system without dual booting. This can make it easier to install and run software that is more easily installed on linux machines (as many of these programs are).

###git

For Debian/Ubuntu

	sudo apt-get install git
	
Instruction for mac and other operating systems can be found at http://git-scm.com/book/en/v2/Getting-Started-Installing-Git. The default download for mac only works for OS X 10.9 and above. For older versions of OS X, download the corresponding git package from http://sourceforge.net/projects/git-osx-installer/files/ and click through the installer. Check to make sure that you have git by typing

	which git
	git

It should give you the pathway of the git executable, and a list of git commands.

cd into your home directory. Clone this git repository:
	
	cd ~
	git clone https://yangya@bitbucket.org/yangya/phylogenomic_dataset_construction.git
	
A directory named phylogenomic_dataset_construction should be copied to your home directory. 

###TransDecoder
Create a directory called "apps" in the home directory and download apps by cloning git repositories. Install TransDecoder.

	mkdir ~/apps
	cd ~/apps
	git clone https://github.com/TransDecoder/TransDecoder.git
	cd ~/apps/TransDecoder
	make
	
If you see an error message:

	cdhit-common.c++:36:9: fatal error: 'omp.h' file not found
	
Here's some info on how to work around it https://groups.google.com/forum/#!msg/transdecoder-users/95ql1tK2QLo/rzn_XtIDPQkJ. Since cd-hit is very fast it's ok if you don't have openMP working.

Alternatively, if you have a gnu compiler, you can do the following:

	cd TransDecoder/transdecoder_plugins
	tar -xvzf cd-hit-v4.6.1-2012-08-27.tgz
	cd cd-hit-v4.6.1-2012-08-27

In the Makefile there, change:

	CC = g++ -Wall -ggdb
	CC = g++ -pg
	CC = g++

(where mac uses clang by default) to something like (I have the gnu compiler g++-4.8):

	CC = g++-4.8 -Wall -ggdb
	CC = g++-4.8 -pg
	CC = g++-4.8

and finally:

	make openmp=yes && make install PREFIX=../../util/bin

and you should be good to go.

###python
Check the version of your python by typing

	python --version
	
Code is tested in python 2.7.x and is compatible with python 2.6. However, it won't work with 3.x.

###blast+ package
Download the tar ball ends with universal-macoscx.tar.gz for mac and the one ends with -x64-linux.tar.gz for linux from ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/. Unzip the downloaded tar ball. This will create a new directory named ncbi-blast-2.2.30+. Move this directory to ~/apps. Do not use apt-get for linux. It will install an older version that works differently than the latest version.

Copy the executables to the PATH:
	
	sudo cp ~/apps/ncbi-blast-2.2.30+/bin/makeblastdb /usr/bin
	sudo cp ~/apps/ncbi-blast-2.2.30+/bin/blast? /usr/bin
	
Now all the blast excutables should be in the PATH. Type
	
	makeblastdb
	blastp -v

Each command should print out a list of command line arguments. 

###mcl
Go to http://micans.org/mcl, click "License & software" on the left side bar, and click "Download a a ready-to-install tarball containing the MCL-edge software". Download the file mcl-latest.tar.gz, unzip it, and move the directory mcl-14-137 to ~/apps. Install the executable to your PATH.

	cd ~/apps/mcl-14-137
	./configure --prefix=/usr
	sudo make install

Type
	
	mcl -h

to make sure that it is correctly installed in the PATH and see a list of commands

###java

Type java to check whether you have java installed. If not, for linux

	sudo apt-get install default-jre

For mac, follow instructions here https://helpx.adobe.com/x-productkb/global/install-java-jre-mac-os.html

###phyutility
Download phyutility from https://code.google.com/p/phyutility/downloads/list. Unzip, and move the directory to ~/apps.

Put phyutility in the PATH. First, get the absolute path for the phyutility.jar file. In mac, you can just drag the .jar file into your terminal to get the absolute path. For example, mine is /Users/yangya/apps/phyutility/phyutility.jar. Next, make a new text file named "phyutility" in your home directory. For mac users, use TextWrangler for the text editing since TextEdit will not be able to save plain text files. Write the following line in this new text file pointing to the absolute path of the jar file, such as:

	java -Xmx6g -jar /Users/yangya/apps/phyutility/phyutility.jar $*

Of course use the actual absolute path on your machine. Make the phyutility file executable

	chmod u+x ~/phyutility

Move the text file to the PATH

	sudo mv ~/phyutility /usr/bin/phyutility

Type 

	phyutility

to make sure that it is correctly installed. It should print out a list of commands.

###mafft
For mac, download the standard package from http://mafft.cbrc.jp/alignment/software/macosx.html and click through the installation.

For linux, download the .rpm file from http://mafft.cbrc.jp/alignment/software/linux.html.

	cd ~/Downloads/ 
	sudo rpm -Uvh mafft-7.221-gcc_fc6.x86_64.rpm 

Again, type 

	mafft -h

to make sure that the executable named mafft is correctly installed in the PATH.

###raxml
Clone the raxml git repository.

	cd ~/apps
	git clone https://github.com/stamatak/standard-RAxML.git
	
Follow the instructions in the README file for choosing which makefile to use for compilation. After compilation, you will get an executable file. Mine is named raxmlHPC-PTHREADS-SSE3 on my older laptop and raxmlHPC-PTHREADS-AVX on my brand new laptop. Copy the executable to your path as a file named "raxml" and make it executable

	sudo cp <path to raxml executable> /usr/bin/raxml
	chmod u+x /usr/bin/raxml

Again, type

	raxml -h

to see the list of commands.

###swipe
Clone the git repository

	cd ~/apps
	git clone https://github.com/torognes/swipe.git
	
Try running the executable named swipe in either the Mac or the Linux directory.

	cd ~/apps/swipe/<Linux or Mac directory>
	./swipe -h

If it works, copy the correct executable to the PATH

	sudo cp swipe /usr/bin/swipe
	
If you have a 6-year-old mac as I do, the mac executable may not work and compiling from source may not work either. Try download an older version of swipe from https://github.com/torognes/swipe/releases, and see whether the executable from an older version would work, or you can compile it from the source code by

	cd <swipe directory>
	make
	sudo cp swipe /usr/bin/swipe

###phyparts
Clone the git repository

	cd ~/apps
	git clone https://bitbucket.org/blackrim/phyparts.git
	
Similar to phyutility, create a text file in the PATH that points to the absolute path of the .jar file, make it executable and test it.

You will also need programs for visualizing alignments and trees. Use any of your favorite ones. Jalview http://www.jalview.org/ is a nice one for visualizing large alignment although we won't encounter large alignment for the tutorial. Figtree is good for visualizing trees. 

For the tutorial we won't generate very large alignments or trees. However, for real data sets it's nice to have programs such as FastTree and PASTA/UPP to handle homologs with more than 1000 sequences.
